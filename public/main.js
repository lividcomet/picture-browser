var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var document = window.document;

function createIndexHTML(document) {
  var i;
  function getOld(callback) {
    let req = new XMLHttpRequest();
    req.open(
      "GET",
      "https://api.jsonbin.io/b/5c063f901deea01014bd047e/latest",
      true
    );
    req.onload = function() {
      while (req.status != 200) {
        console.log("I <3 Javascript");
      }
      var json = JSON.parse(req.responseText);
      callback(json);
    };
    req.send();
  }

  getOld(json => {
    function createIMG() {
      for (i = 0; i < json.links.length; i++) {
        link = JSON.parse(json.links[i]);
        // https://stackoverflow.com/questions/19988971/create-multiple-img-tags-in-a-div-through-the-dom
        var img = document.createElement("img");
        img.src = link.url;
        img.className = "img-fluid";
        document.getElementById("div" + i).style =
          "background-image: url('" +
          link.url +
          "'); background-size: contain;";
      }
    }

    function createDIV() {
      for (i = 0; i < json.links.length; i++) {
        link = JSON.parse(json.links[i]);
        var divWrapper = document.createElement("div");
        divWrapper.setAttribute("onclick", "window.open('" + link.url + "');");
        divWrapper.classList = "col-md-6";
        divWrapper.setAttribute("id", "div" + i);
        document.body.querySelector("main").appendChild(divWrapper);
      }
    }
    createDIV();
    createIMG();
  });
}
createIndexHTML(document);
